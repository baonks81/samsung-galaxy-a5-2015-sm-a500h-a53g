﻿Interactive:

above_hispeed_delay : 100000 800000:80000 998400:100000
boostpulse_duration : 10000
hispeed_freq : 200000
max_freq_hysteresis : 0
target_loads : 59 400000:60 533000:70 800000:80 998400:90 1094400:100
timer_slack : -1
use_sched_load : 0
align_windows : 1
boost : 0
go_hispeed_load : 99
io_is_busy : 0
min_sample_time : 40000
timer_rate : 30000
use_migration_notif : 0